#!/usr/bin/env python3

# -------------------------------
# projects/collatz/TestCollatz.py
# Copyright (C) 
# Glenn P. Downing
# -------------------------------

# https://docs.python.org/3.6/reference/simple_stmts.html#grammar-token-assert_stmt

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Diplomacy import diplomacy_solve

# -----------
# TestCollatz
# -----------


class TestCollatz (TestCase):

    # -----
    # solve
    # -----

    def test_solve1(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB [dead]\nC [dead]\n")
    def test_solve2(self):
        r = StringIO("A Madrid Support C\nB Barcelona Support A\nC London Support B")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A Madrid\nB Barcelona\nC London\n")
    def test_solve3(self):
        r = StringIO("A Madrid Move Barcelona\nB Barcelona Move London\nC London Move Madrid")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A Barcelona\nB London\nC Madrid\n")
    def test_solve4(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Support B")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB Madrid\nC London\n")
    def test_solve5(self):
        r = StringIO("A Madrid Support C\nB Barcelona Move Madrid\nC London Move Austin\nD Austin Support B")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB [dead]\nC [dead]\nD [dead]\n")

# ----
# main
# ----


if __name__ == "__main__": #pragma: no cover
    main()


